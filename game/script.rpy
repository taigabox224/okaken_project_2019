define o = Character('海淵寺　万里(かいえんじ　まり)', color="#FF0000")
define no = Character('???', color="#FF0000")
define j = Character('百瀬　鈴子(ももせ　りんこ)', color="#008000")
define nj = Character('???', color="#008000")

label start:

    scene bg_station
    play music "music/semi.mp3" fadeout 3.0
    with fade

    "これは本当にあった出来事である"

    "ここは某県の廃村＿＿＿"

    "かつては炭鉱街として栄えたが、バブル崩壊と同時に過疎化が激化し、"

    "今となっては忘れ去られた土地となってしまった。"

    "しかし今日、そんな寂れた廃駅に、真夏の日差しが二つの影の落とした。"

    stop music fadeout 1.0

    pause 1.0

    no "えーーーーーーーっと......"

    no "確かこの辺だと思うんだけど＿＿＿＿"

    nj "ぜえ...ぜえ..."

    nj "あ...あの...もう少しゆっくり..."

    pause 1.0

    play music "music/bgm_main.ogg"

    show mari main 0001
    with dissolve

    o "あった！！これが例の幽霊の出るって噂の廃駅よっ！！！"

    nj "ちょ...あの...センパイ..."

    show mari thought 0001
    o "駆け落ちを邪魔されてこの駅で二人で自刃したのよね...なんて情熱的で悲劇的なのかしら..."

    nj "...センパイ...センパイってば！"

    show rinko main 0001 at left
    with dissolve

    j "やっと追いつきましたよ..."

    show mari meta 0001

    o "あっ地味子ちゃんやっと追いついたの？おっそーい！！"

    show rinko amazed 0001 at left

    j "...この格好の人間に無茶言わんでくださいよ..."

    show rinko question 0001 at left
    show mari main 0001

    j "...というかなんで私たち二人だけなんです...?"

    stop music fadeout 2.0
    pause 2.0

    play music "music/bgm_serious.ogg" fadein 1.0

    show mari fear 0001

    o "＿＿＿＿地味子ちゃん...それ___本気で言ってるの...?"

    show rinko tired 0001 at left

    j "え?...ええ?"

    o "本当に...わからないの...?"

    show rinko tired 0001 at left

    j "え...センパイ...一体何を....!?"

    o "そんなの決まってるじゃない..."

    stop music fadeout 1.0
    pause 2.0

    play music "music/bgm_main.ogg"

    show mari meta 0001

    o "会長のキャラをまだよく掴みきれてないからよ"

    show rinko main 0001 at left

    j "だから唐突なメタ発言やめてください"

    show mari doya 0001

    o "とりあえず「あらあらうふふ...」とだけ喋らせようかと思ったけど..."

    show mari meta 0001

    o "やめといたわ！"

    show rinko angry 0001

    j "英断ですね"

    show rinko question 0001 at left

    j "えーと......まあそれはそれとして___"

    show rinko main 0001 at left

menu:

    "今日はどんな怪異を追いかけるんです?":
        jump kaii

    "ここが例の駆け落ちに失敗した男女が心中したっていう...?":
        jump kakeochi

label kaii:

    show mari doya 0001

    o "おやおや〜?生粋のオカルトマニアの地味子ちゃんもこの情報は掴んでなかったみたいね?"

    o "この情報を掴むのにどれほどの夜明けを迎えたかっ！"

    show rinko amazed 0001 at left

    j "テスト期間中に何やってるんですか"

    show mari main 0001

    o "まあ簡単に言うと＿＿＿＿＿"

    jump end

label kakeochi:

    show mari main 0001

    o "おー！さすがは我が同好会一のオカルトマニア！！さっすがー！！"

    o "そしたら言うまでもないかもだけど______"

    jump end

label end:

    stop music fadeout 1.0
    scene bg meadow
    with fade
    pause 1.0
    play music "music/bgm_serious.ogg" fadein 1.0


    "この時はまだわかっていなかった"

    "まさかこの後__________あんな恐ろしい目に会うなんて_____"
